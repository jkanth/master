
<?php 
if(isset($_POST['submit'])){
    $to = "jayakanth.b199@gmail.com.com"; // this is your Email address
    $from = $_POST['email']; // this is the sender's Email address
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $subject = "Form submission";
    $subject2 = "Copy of your form submission";
    $message = $first_name . " " . $last_name . " wrote the following:" . "\n\n" . $_POST['message'];
    $message2 = "Here is a copy of your message " . $first_name . "\n\n" . $_POST['message'];

    $headers = "From:" . $from;
    $headers2 = "From:" . $to;
    mail($to,$subject,$message,$headers);
    mail($from,$subject2,$message2,$headers2); // sends a copy of the message to the sender
    echo "Mail Sent. Thank you " . $first_name . ", we will contact you shortly.";
    // You can also use header('Location: thank_you.php'); to redirect to another page.
    }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="viewport" content="width=device-width, inital-scal=1.0" />
    <meta charset="UTF-8" />
    <link rel="stylesheet" type="text/css" href="vendor/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="vendor/css/Grid.css" />
    <link rel="stylesheet" type="text/css" href="vendor/css/ionicons.min.css" />
    <link rel="stylesheet" type="text/css" href="resource/css/style.css" />
    <link rel="stylesheet" type="text/css" href="resource/css/queries.css" />
    <link
      href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;1,300&display=swap"
      rel="stylesheet"
    />
    <title>Comfort Work</title>
  </head>
  <body>
    <header>
      <nav>
        <div class="row">
          <img src="resource/img/comfort.png" alt="comfort logo" class="logo" />
          <p class="logo-title">Comfort Work</p>
          <img
            src="resource/img/logo.png"
            alt="omnifood logo"
            class="logo-black"
          />
          <ul class="main-nav">
            <li><a href="#">Home</a></li>
            <li><a href="#About">AboutUs</a></li>
            <li><a href="#clients">Our clintes</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div>
      </nav>
      <div class="hero-text-box">
        <h1>Work N Style.</h1>
        <!-- <a class="btn btn-full" href="#">I’m hungry</a>
        <a class="btn btn-ghost" href="#">Show me more </a> -->
      </div>
    </header>

     <section class="section-features js--section-features" id="About">
        <div class="row">
          <h2></h2>
        </div>
        <div class="row">
        <div class="col span-1-of-2">
            <p class="long-copy">Bangalore’s CBD- Central Business District is the heart of Bangalore
              with great Colonial historic roads like South Parade (now M.G. Road),
              St. Mark’s Road and Church Street. These above roads have witnessed
              the grandeur of living styles of Indian and English aristocrats
              frequenting these roads for food fashion and socializing. This zone of
              Bangalore saw Victorian architectured Bungalows, elegantly constructed
              churches, classy clubs, well know Anglo- Indian schools and movie
              halls.
              Now the past glory continues with change of generation, most of the
              above mentioned inventories continue with replacement to modern
              building’s, high end shopping centers, malls in the CBD. Church street
              has become the high street of Bangalore with shops and multi cuisine
              restaurants, pubs, and hangout places of the hi-fi hep people of
              Bangalore.</p>
          </div>
          <div class="col span-1-of-2 ab-photo">
            <img src="resource/img/bangalore-vidhana-soudha-152459191465-orijgp.jpg">
          </div>
        </div>
          
   <section class="section-plans" id="clients">
      <div class="row">
        <h2>Our Coworking Space.</h2>
      </div>
      <div class="row">
        <div class="col span-1-of-2">
          <div class="plan-box">
            <div>
              <h3>Flexible seats</h3>

              <!-- <p class="plan-price">399$<span>/meal</span></p>
              <p class="plan-price-meal">That's only 13.99$ per meal</p> -->
            </div>
            <div>
              <ul>
                <li>
                  <i class="ion-ios-checkmark-empty icon-small"></i><p class="cowork-space">Let the
                  working ideas soar with comfort work </p>
                </li>
                <li>
                  <i class="ion-ios-checkmark-empty icon-small"></i> <p class="cowork-space">Choose a hot
                  desks set work stations for the day and start working.
                </p>
                </li>
                <li>
                  <i class="ion-ios-checkmark-empty icon-small"></i><p class="cowork-space">Work with
                  coworkers /new people and benefit from the diversity in the
                  community.</p>
                </li>
                <li>
                  <i class="ion-ios-checkmark-empty icon-small"></i><p class="cowork-space">Collaborate
                  & network.</p>
                </li>
                <li>
                  <i class="ion-ios-checkmark-empty icon-small"></i><p class="cowork-space">Book on
                  daily to yearly basis and get complimentary high speed Wi-Fi,
                  hot coffee and printing credits </p>
                </li>
              </ul>
            </div>
            <div>
              <a href="#" class="btn btn-full" onclick="openForm()">BookNow</a>
            </div>
          </div>
        </div>
        <div class="col span-1-of-2">
          <div class="plan-box">
            <div>
              <h3>Meeting rooms</h3>
              <!-- <p class="plan-price">149$<span>/meal</span></p>
              <p class="plan-price-meal">That’s only 14.90$ per meal</p> -->
            </div>
            <div>
              <ul>
                <li>
                  <i class="ion-ios-checkmark-empty icon-small"></i><p class="cowork-space">To make a
                  next big idea.</p>
                </li>
                <li>
                  <i class="ion-ios-checkmark-empty icon-small"></i><p class="cowork-space">Host your
                  clients inside our designed meeting rooms Anytime</p>
                </li>
                <li>
                  <i class="ion-ios-checkmark-empty icon-small"></i><p class="cowork-space">Enjoy true
                  hospitality with in-meeting service of tea or coffee on hourly
                  basis with complimentary high speed wifi and printing credits.</p>
                </li>
                <br />
                <br />
                <br />
              </ul>
            </div>
            <div>
              <a href="#" class="btn btn-full" onclick="openForm()">BookNow</a>
            </div>
          </div>
        </div>
       </section>
    <section class="section-plans">
      <div class="row">
        <div class="col span-1-of-2">
          <div class="plan-box">
            <div>
              <h3>Fixed seats/ Dedicated seats</h3>

              <!-- <p class="plan-price">399$<span>/meal</span></p>
              <p class="plan-price-meal">That's only 13.99$ per meal</p> -->
            </div>
            <div>
              <ul>
                <li>
                  <i class="ion-ios-checkmark-empty icon-small"></i><p class="cowork-space">A fully furnished dedicated desk with a secure storage space, pin-up board and white board.
                  </p></li>
                <li>
                  <i class="ion-ios-checkmark-empty icon-small"></i><p class="cowork-space">Ideal  for individuals and team.
                  </p></li>
                <li>
                  <i class="ion-ios-checkmark-empty icon-small"></i><p class="cowork-space">Complimentary high  speed wifi , printing and meeting room credits
                  </p></li>
                <br />
                <br />
                <br />
                <br/>
                <br/>
                </ul>
            </div>
            <div>
              <a href="#" class="btn btn-full" onclick="openForm()">BookNow</a>
            </div>
          </div>
        </div>
        <div class="col span-1-of-2">
          <div class="plan-box">
            <div>
              <h3>Premium Cabins</h3>
            </div>
            <div>
              <ul>
                <li>
                  <i class="ion-ios-checkmark-empty icon-small"></i><p class="cowork-space">Plug & play cabins for a team of 2 - 6  to provide a focus on  work  and grow your company.
                  </p></li>
                <li>
                  <i class="ion-ios-checkmark-empty icon-small"></i><p class="cowork-space">Private office with lockable cabins, storage space, and white boards.
                  </p> </li>
                <li>
                  <i class="ion-ios-checkmark-empty icon-small"></i><p class="cowork-space">Personalize with your company logo 
                  </p></li>
                <li>
                  <i class="ion-ios-checkmark-empty icon-small"></i> <p class="cowork-space">Book on monthly to yearly basis. 
                  </p></li>
                <br />
                <br />
                <br />
                <br/>
              </ul>
            </div>
            <div>
              <a href="#" class="btn btn-full" onclick="openForm()">BookNow</a>
            </div>
          </div>
        </div>
    </section>
      
    
   

    <section>
      <div class="row">
<h2>Our Clients</h2>
      </div>
      <div class="row">
        <div class="col span-1-of-6 box clients ">
          <h3>MANTHAN SOFTWARE SERVICES</h3>
        </div>
        <div class="col span-1-of-6 box clients">
          <h3> NETWORK OBJECTS TECHNOLOGIES</h3>
        </div>
        <div class="col span-1-of-6 box clients">
          <h3>MAKEOVER DIARIES</h3>
        </div>
        <div class="col span-1-of-6 box clients">
          <h3>CONNSOCIO TECHNOLOGIES </h3>
        </div>
        <div class="col span-1-of-6 box clients">
          <h3>MERA PUPPY</h3>
        </div>
        <div class="col span-1-of-6 box clients">
          <h3>ENGCES ELECTROTECH SOLUTION</h3>
        </div>
      </div>
      <div class="row">
        <div class="col span-1-of-6 box clients ">
          <h3>TINKERTORQ INNOVATIONS</h3>
        </div>
        <div class="col span-1-of-6 box clients">
          <h3> SKILLZ
            KRAFTERS</h3>
        </div>
        <div class="col span-1-of-6 box clients">
          <h3>THINQ ADVERTISING </h3>
        </div>
        <div class="col span-1-of-6 box clients">
          <h3>GLOBOSS IMPEX PVT LTD</h3>
        </div>
        <div class="col span-1-of-6 box clients">
          <h3>SMB SURE BUSINESS SOLUTIONS </h3>
        </div>
        <div class="col span-1-of-6 box clients">
          <h3>PRATHIBANVITHA INFOSOURCE</h3>
        </div>
      </div>
        
    </section>


    <!-- <section class="section-test">
      <div class="row">
        <h2>Our customers can't live without us</h2>
      </div>
      <div class="row">
        <div class="col span-1-of-3">
          <blockquote>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas, reiciendis odio dolores beatae expedita dolorem aliquid tempora deserunt deleniti accusamus eos labore, eveniet, qui animi pariatur adipisci. Possimus, quae impedit.
            <cite
              > Alberto
              Duncan</cite
            >
          </blockquote>
        </div>
        <div class="col span-1-of-3">
          <blockquote>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic commodi in, ipsam suscipit saepe, est vero odio eius nisi iusto voluptas perferendis neque dignissimos tempora enim ex reiciendis! Voluptatibus, facere!

            <cite
              >Joana
              Silva</cite
            >
          </blockquote>
        </div>
        <div class="col span-1-of-3">
          <blockquote>
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Totam recusandae deleniti sequi, labore, non illum perferendis blanditiis nihil molestias voluptates numquam molestiae magni harum nulla ducimus illo exercitationem veritatis doloribus!
            <cite
              > Milton
              Chapman</cite
            >
          </blockquote>
        </div>
      </div>
    </section> -->

    <section class="section-meals">
      <ul class="meals-showcase clearfix">
        <li>
          <figure class="meal-photo">
            <img src="resource/img/20190307_115845-1024x498.jpg" alt="" />
          </figure>
        </li>
        <li>
          <figure class="meal-photo">
            <img src="resource/img/20190323_152153-1024x498.jpg" alt="" />
          </figure>
        </li>
        <li>
          <figure class="meal-photo">
            <img src="resource/img/20190331_132800-1024x498.jpg" alt="" />
          </figure>
        </li>
        <li>
          <figure class="meal-photo">
            <img src="resource/img/20190501_125917-1024x498.jpg" alt="" />
          </figure>
        </li>
      </ul>
      <ul class="meals-showcase clearfix">
        <li>
          <figure class="meal-photo">
            <img src="resource/img/20190501_125118-1024x498.jpg" alt="" />
          </figure>
        </li>
        <li>
          <figure class="meal-photo">
            <img src="resource/img/20190501_125145-1024x498.jpg" alt="" />
          </figure>
        </li>
        <li>
          <figure class="meal-photo">
            <img src="resource/img/20190501_125917-1024x498.jpg" alt="" />
          </figure>
        </li>
        <li>
          <figure class="meal-photo">
            <img src="resource/img/20190501_130534-1024x498.jpg" alt="" />
          </figure>
        </li>
      </ul>
    </section>


    <div class="form-popup" id="myForm">
      <form action="mailto:Info@comfortwork.in , mahalakshmi@comfortwork.in" method="post" enctype="text/plain" class="form-container">
        <h2>BookNow</h2>
    
        <label for="name"  ><b>Name:</b></label>
        <input type="text" placeholder="Enter Name" name="name" id="form-name" required>
    
        <label for="email"><b>Email:</b></label>
        <input type="email" placeholder="Enter email" name="email" id="form-email" required>
        <label for="number"><b>ContactNum:</b></label>
        <input type="number" placeholder="" name="number" id="form-num" required>
    
        <button type="submit" class="btn">BookNow</button>
        <button type="button" class="btn cancel" onclick="closeForm()">Close</button>
      </form>
    </div>


    
     <section class="section-form" id="contact">
      <div class="row">
        <h2>We are happy to hear form you</h2>
      </div>
      <div class="row">
        <form method="post" action="#" class="contact-form">
          <div class="row">
            <div class="col span-1-of-3">
              <label for="name">Name</label>
            </div>
            <div class="col span-2-of-3">
              <input
                type="text"
                name="name"
                id="name"
                placeholder="your name"
                required
              />
            </div>
          </div>
          <div class="row">
            <div class="col span-1-of-3">
              <label for="email"> Email</label>
            </div>
            <div class="col span-2-of-3">
              <input
                type="email"
                name="email"
                id="email"
                placeholder="your email"
                required
              />
            </div>
          </div>
          <div class="row">
            <div class="col span-1-of-3">
              <label for="find-us">How did you find us</label>
            </div>
            <div class="col span-2-of-3">
              <select name="find-us" id="find-us">
                <option value="friends">friends</option>
                <option value="search" selected>Search Engine</option>
                <option value="others">others</option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col span-1-of-3">
              <label>NewsLetter</label>
            </div>
            <div class="col span-2-of-3">
              <input type="checkbox" name="news" id="news" checked />Yes,please
            </div>
          </div>
          <div class="row">
            <div class="col span-1-of-3">
              <label>Drop us a line</label>
            </div>
            <div class="col span-2-of-3">
              <textarea name="message" placeholder="your message"></textarea>
            </div>
          </div>
          <div class="row">
            <div class="col span-1-of-3">
              <label>&nbsp;</label>
            </div>
            <div class="col span-2-of-3">
              <input type="submit" value="send it" />
            </div>
          </div>
        </form>
      </div>
      <br>
      <p><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3887.9370653131377!2d77.59966521484951!3d12.975877118295235!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae17a43eb7f2e3%3A0x1bdf6408a0df482!2sCOMFORT%20WORK!5e0!3m2!1sen!2sin!4v1598868542267!5m2!1sen!2sin" width="100%" height="500" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></p>
    </section>
    <footer>
      <div class="row">
        <div class="col span-1-of-2">
          <ul class="footer-nav">
            <li><a href="#">Home</a></li>
            <li><a href="#">Client</a></li>
            <li><a href="#">Contact</a></li>
            <li><a href="#">AboutUs</a></li>
          </ul>
        </div>
        <div class="col span-1-of-2">
          <ul class="social-links">
            <li>
              <a href="#"><i class="ion-social-facebook"></i></a>
            </li>
            <li>
              <a href="#"><i class="ion-social-twitter"></i></a>
            </li>
            <li>
              <a href="#"><i class="ion-social-googleplus"></i></a>
            </li>
            <li>
              <a href="#"><i class="ion-social-instagram"></i></a>
            </li>
          </ul>
        </div>
      </div>
      <div class="row">
        <p>copyright &copy;2020 <span class="footer-right">Comfort Work</span>. All rights reserved.</p>
      </div>
    </footer>

    <script src="//cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script>
    <script src="//cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="//cdn.jsdelivr.net/selectivizr/1.0.3b/selectivizr.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="resource/js/script.js"></script>
    <script src="vendor/js/jquery.waypoints.min.js"></script>
    <script src="/path/to/noframework.waypoints.min.js"></script>
    <script>
      function openForm() {
        document.getElementById("myForm").style.display = "block";
        document.getElementById("form-name").value = '';
        document.getElementById("form-email").value = '';
        document.getElementById("form-num").value = '';
      }
      
      function closeForm() {
        document.getElementById("myForm").style.display = "none";
      
      }
      </script>
  <!-- Code injected by live-server -->
<script type="text/javascript">
	// <![CDATA[  <-- For SVG support
	if ('WebSocket' in window) {
		(function () {
			function refreshCSS() {
				var sheets = [].slice.call(document.getElementsByTagName("link"));
				var head = document.getElementsByTagName("head")[0];
				for (var i = 0; i < sheets.length; ++i) {
					var elem = sheets[i];
					var parent = elem.parentElement || head;
					parent.removeChild(elem);
					var rel = elem.rel;
					if (elem.href && typeof rel != "string" || rel.length == 0 || rel.toLowerCase() == "stylesheet") {
						var url = elem.href.replace(/(&|\?)_cacheOverride=\d+/, '');
						elem.href = url + (url.indexOf('?') >= 0 ? '&' : '?') + '_cacheOverride=' + (new Date().valueOf());
					}
					parent.appendChild(elem);
				}
			}
			var protocol = window.location.protocol === 'http:' ? 'ws://' : 'wss://';
			var address = protocol + window.location.host + window.location.pathname + '/ws';
			var socket = new WebSocket(address);
			socket.onmessage = function (msg) {
				if (msg.data == 'reload') window.location.reload();
				else if (msg.data == 'refreshcss') refreshCSS();
			};
			if (sessionStorage && !sessionStorage.getItem('IsThisFirstTime_Log_From_LiveServer')) {
				console.log('Live reload enabled.');
				sessionStorage.setItem('IsThisFirstTime_Log_From_LiveServer', true);
			}
		})();
	}
	else {
		console.error('Upgrade your browser. This Browser is NOT supported WebSocket for Live-Reloading.');
	}
	// ]]>
</script></body>
</html>
